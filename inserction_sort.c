/* Autor: Allen Konstanz
 * Web: allenkonstanz.blogspot.com
 * Adaptado por Jackson Douglas, em 04/12/2018 */

#include <stdio.h>

void insertionSortD(int array[], int tamanho) {
    //Decrescente
      int i, j, tmp;
      for (i = 1; i < tamanho; i++) {
            j = i;
            while (j > 0 && array[j - 1] < array[j]) {
            //Percorre o array do �ltimo ao primeiro elemento, comparando os elementos 1 a 1.
            //Caso encontre um n�mero menor na posi��o que antecede � atual, substitui pelo autal.
                  tmp = array[j];
                  array[j] = array[j - 1];
                  array[j - 1] = tmp;
                  j--;
            }
      }
}
void insertionSortC(int array[], int tamanho) {
    //Crescente
      int i, j, tmp;
      for (i = 1; i < tamanho; i++) {
            j = i;
            while (j > 0 && array[j - 1] > array[j]) {
            //Percorre o array do �ltimo ao primeiro elemento, comparando os elementos 1 a 1.
            //Caso encontre um n�mero maior na posi��o que antecede � atual, substitui pelo autal.
                  tmp = array[j];
                  array[j] = array[j - 1];
                  array[j - 1] = tmp;
                  j--;
            }
      }
}

int main(int argc, char** argv)
{
   int array[100], tamanho, ordem;
   printf("\n\nEntre com o numero de termos...: ");
   scanf("%d", &tamanho);
   printf("\nEntre com os elementos do array...:");
   for(int i = 0; i < tamanho;i++){
      printf("\n\t\tDigite o %do numero: ",i+1);
      scanf("%d",&array[i]);
   }
   printf("\n[1] Ordernar array em ordem crescente\n[2] Ordenar array em ordem decrescente\nResposta...: ");
   scanf("%d",&ordem);
   if (ordem == 1){
      insertionSortC(array,tamanho);
      printf("\nArray em ordem crescente: ");
      for(int i=0; i<tamanho; i++){
      printf(" [%d]", array[i]);
   }

      }else if (ordem ==2) {
   insertionSortD(array,tamanho);
   printf("\nArray em ordem decrescente: ");
   for(int i=0; i<tamanho; i++){
      printf(" [%d]", array[i]);
   }
}
   return 0;
}
