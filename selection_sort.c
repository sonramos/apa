#include <stdio.h>

void selection_sort (int vetor[],int max) {
  int i, j, min, tmp;

  for (i = 0; i < (max - 1); i++) {
    /* O minimo � o primeiro n�mero n�o ordenado ainda */
    min = i;
    for (j = i+1; j < max; j++) {
      /* Caso tenha algum numero menor ele faz a troca do minimo*/
      if (vetor[j] < vetor[min]) {
   min = j;
      }
    }
    /* Se o minimo for diferente do primeiro numero n�o ordenado ele faz a troca para ordena-los*/
    if (i != min) {
      tmp = vetor[i];
      vetor[i] = vetor[min];
      vetor[min] = tmp;
    }
  }
  /* Imprime o vetor ordenado */
  for (i = 0; i < max; i++) {
    printf ("%d ",vetor[i]);
  }
  printf ("\n");
}

int main(int argc, char** argv){
  int max, i;
  /* L� o m�ximo de algarismos do vetor*/
    printf("Insira o tamanho do vetor: ");
    scanf ("%d",&max);

    int  vetor[max];
    printf("\nEntre com os elementos do vetor...:");
    for(int i = 0; i < max; i++){
      printf("\n\t\tDigite o %do numero: ",i+1);
      scanf("%d",&vetor[i]);
    }

  selection_sort (vetor, max);

}
